#!/bin/bash

cd '/Users/simen/Documents/uio/generic_fec/fec/testProgs/'
for L in `seq 1 2`;
    do
        for D in `seq 1 2`;
            do
                echo 'L:'$L 'D:'$D
                ./testH264VideoStreamer $L $D
                echo 'Sleeping for 20 seconds.'
                sleep 20s
            done
    done
