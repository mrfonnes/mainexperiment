const fs = require('fs')
const pcapp = require('pcap-parser')

module.exports = {
    parsePcap: function(inputFile) {
        return new Promise((resolve, reject) => {
            const parser = pcapp.parse(inputFile)
            let packets = []
            let globalHeader

            parser.on('globalHeader', header => {
                globalHeader = header
            })

            parser.on('packet', packet => {
                packets.push(packet)
            })

            parser.on('end', () => {
                resolve({packets, globalHeader})
            })
        })
    },

    writePcap: function(outputFile, globalHeader, packets) {
        return new Promise((resolve, reject) => {
            const fileSize = 24 + packets.reduce((acc, e) => acc + 16 + e.data.length, 0)

            let buffer = Buffer.alloc(fileSize)
            buffer.writeUInt32LE(globalHeader.magicNumber, 0)
            buffer.writeUInt16LE(globalHeader.majorVersion, 4)
            buffer.writeUInt16LE(globalHeader.minorVersion, 6)
            buffer.writeInt32LE(globalHeader.gmtOffset, 8)
            buffer.writeUInt32LE(globalHeader.timestampAccuracy, 12)
            buffer.writeUInt32LE(globalHeader.snapshotLength, 16)
            buffer.writeUInt32LE(globalHeader.linkLayerType, 20)

            let offset = 24;
            packets.forEach(e => {
                buffer.writeUInt32LE(e.header.timestampSeconds, offset)
                offset += 4
                buffer.writeUInt32LE(e.header.timestampMicroseconds, offset)
                offset += 4
                buffer.writeUInt32LE(e.header.capturedLength, offset)
                offset += 4
                buffer.writeUInt32LE(e.header.originalLength, offset)
                offset += 4

                e.data.forEach(b => buffer[offset++] = b)
            })
            fs.writeFile(outputFile, buffer, (err) => {
                if (err) reject(err)
                else resolve()
            })
        })
    }
}
