const fs = require('fs')
const pcapp = require('pcap-parser')

module.exports = (input, output, n, excludeStart) => {
    return new Promise((resolve, reject) => {
        const parser = pcapp.parse(input)
        let packets = []
        let globalHeader
        let size = 0
        let counter = 0

        const packetHeaderSize = 16

        parser.on('globalHeader', header => {
            globalHeader = header
        })

        parser.on('packet', packet => {
          const random = Math.random()
          if (counter <= excludeStart) {
              packets.push(packet)
              size += packetHeaderSize + packet.data.length
              counter++
              return
          }
          if (random > n) return
          packets.push(packet)
          size += packetHeaderSize + packet.data.length
          counter++
        })

        parser.on('end', () => {
            let total = size + 24
            var buffer = Buffer.alloc(total)
            buffer.writeUInt32LE(globalHeader.magicNumber, 0)
            buffer.writeUInt16LE(globalHeader.majorVersion, 4)
            buffer.writeUInt16LE(globalHeader.minorVersion, 6)
            buffer.writeInt32LE(globalHeader.gmtOffset, 8)
            buffer.writeUInt32LE(globalHeader.timestampAccuracy, 12)
            buffer.writeUInt32LE(globalHeader.snapshotLength, 16)
            buffer.writeUInt32LE(globalHeader.linkLayerType, 20)

            let offset = 24;
            packets.forEach(e => {
                buffer.writeUInt32LE(e.header.timestampSeconds, offset)
                offset += 4
                buffer.writeUInt32LE(e.header.timestampMicroseconds, offset)
                offset += 4
                buffer.writeUInt32LE(e.header.capturedLength, offset)
                offset += 4
                buffer.writeUInt32LE(e.header.originalLength, offset)
                offset += 4

                e.data.forEach(b => buffer[offset++] = b)
            })
            fs.writeFileSync(output, buffer, (err) => {
                if (err) reject()
                else resolve()
            })
        })
    })
}
