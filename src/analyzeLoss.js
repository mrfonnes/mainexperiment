const { parsePcap } = require('./myPcapParser')
const { getPayloadType, getSSRC } = require('./pcaputils')

const rtpOffset = 42

analyze()

// L D Overhead repairWindow Lossbefore Lossafter

/*
const myMap = {
  123: {ssrc: 123, packets: []},
  456: {ssrc: 456, packets: []},
  789: {ssrc: 789, packets: []}
}

etter Object.values(myMap):

[
  {ssrc: 123, packets: []},
  {ssrc: 456, packets: []},
  {ssrc: 789, packets: []}
]*/

async function analyze() {
    console.log("Begin")
    const pcap1Packets = await getPackets(process.argv[2])
    console.log("Gotten pre pcap file")
    const pcap2Packets = await getPackets(process.argv[3])
    console.log("Gotten pcap with loss file")
    const pcap3Packets = await getPackets(process.argv[4])
    console.log("Gotten pcap with fec file")

    pcap1Packets.forEach(e => {
        let stuff = getStuff(e.packets,
            pcap2Packets[e.ssrc].packets,
            pcap3Packets[e.ssrc].packets)

        stuff.forEach(e => {
            console.log(e)
        })
    })
}

function getSeq(packet) {
    return packet.data.readUInt16BE(rtpOffset + 2)
}

function getTimestamp(packet) {
    return (packet.header.timestampSeconds + (packet.header.timestampMicroseconds / 1000000.0)) * 1000.0
}

async function getPackets(pcap) {
    const packets = (await parsePcap(pcap)).packets.filter(e => getPayloadType(e) == 96)
    let myMap = {}

    packets.forEach(packet => {
        let ssrc = getSSRC(packet)

        if (!myMap[ssrc]) {
            myMap[ssrc] = {ssrc, packets: []}
        }
        myMap[ssrc].packets.push(packet)
    })
    return myMap
}

function getStuff(packets1, packets2, packets3) {
    return packets1.map(e => {    //Husk at det er rar loss, at vi kanskje må loope fra 0
        const entry = {seq: getSeq(e), before: {lost: true, timestamp: NaN}, after: {lost: true, timestamp: NaN}}

        const beforePacket = packets2.find(f => getSeq(e) === getSeq(f))
        const afterPacket = packets3.find(f => getSeq(e) === getSeq(f))
        if (beforePacket) {
            entry.before.lost = false
            entry.before.timestamp = getTimestamp(beforePacket)
        }
        if (afterPacket) {
            entry.after.lost = false
            entry.after.timestamp = getTimestamp(afterPacket)
        }
        return entry
    })
}
