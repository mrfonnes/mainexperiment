const rtpOffset = 42

const pcaputils = {
    calculateOverhead: function(L, D) {
        return (1.0 / L) + (1.0 / D);
    },

    makeClusters: function(L, D, packets) {
        let clusters = []

        packets.forEach((e, i) => {
            const index = parseInt(i / (L * D))
            if (!clusters[index]) clusters[index] = []
            clusters[index].push(e)
        })
        return clusters
    },

    getMaxTimestampDiff: function(clusters) {
        return clusters.map(e => {
            const timestamps = e.map(l => (l.header.timestampSeconds + (l.header.timestampMicroseconds / 1000000.0)) * 1000.0)
            return (Math.max(...timestamps) - Math.min(...timestamps))// * 1000
        })
        .sort((e1, e2) => e1 - e2)
        .pop()
    },

    getRTPStream: function(packets) {
        const sourceSsrc = packets[0].data.readUInt32BE(rtpOffset + 8)
        const nonInterleavedSSrc = packets.find(e => pcaputils.getPayloadType(e) == 115)
            .data.readUInt32BE(rtpOffset + 8)
        const interleavedSSrc = packets.find(e => pcaputils.getPayloadType(e) == 116)
            .data.readUInt32BE(rtpOffset + 8)

        return packets.filter(e => [sourceSsrc, nonInterleavedSSrc, interleavedSSrc].includes(pcaputils.getSSRC(e)))
    },

    getPayloadType: function(packet) {
        return packet.data.readUInt8(rtpOffset + 1) & (~0x80)
    },

    getSSRC: function(packet) {
        return packet.data.readUInt32BE(rtpOffset + 8)
    },

    getLAndDValues: function(packets) {
        let values = []
        for (let L = 1; L <= 20; L++) {
            for (let D = 1; D <= 255; D++) {
                const clusters = pcaputils.makeClusters(L, D, packets)
                const repairWindow = pcaputils.getMaxTimestampDiff(clusters)

                let fecOverhead = pcaputils.calculateOverhead(L, D)
                if (repairWindow < 500.0 && fecOverhead < 0.67) {
                    values.push({L, D, repairWindow, fecOverhead})
                }
            }
        }
        return values
    }
}

module.exports = pcaputils
