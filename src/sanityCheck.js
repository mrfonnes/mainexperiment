const { getSSRC, getPayloadType, getLAndDValues } = require('./pcaputils')
const { parsePcap } = require('./myPcapParser')

const rtpOffset = 42

let myObject = {L: 2, D: 6, beforeAvgLoss: 5.34, afterAvgLoss: 1.34, avgLatency: 332}

printSSRCs(process.argv[2], process.argv[3], process.argv[4], process.argv[5])

async function printSSRCs(pcap0, pcap1, pcap2, pcap3) {
    console.log("We began")
    const myPcap0 = await parsePcap(pcap0)
    console.log("Read pcap 0")
    const myPcap1 = await parsePcap(pcap1)
    console.log("Read pcap 1")
    const myPcap2 = await parsePcap(pcap2)
    console.log("Read pcap 2")
    const myPcap3 = await parsePcap(pcap3)
    console.log("Read pcap 3")

    let LAndDs = getLAndDValues(myPcap0.packets)
    console.log("Gotten L and D values")
    let values = getValues(myPcap1.packets)
    console.log("Gotten pcap1 packets")
    let values2 = getValues(myPcap2.packets)
    console.log("Gotten pcap2 packets")
    let values3 = getValues(myPcap3.packets)
    console.log("Gotten pcap3 packets")

    //console.log(Object.values(values)[0].packets);

    let index = 0
    let splitted = Object.values(values).map(e => {
        let stuff = getStuff(
            e.packets,
            values2[e.ssrc].packets,
            values3[e.ssrc].packets
        )
        stuff.L = LAndDs[index].L
        stuff.D = LAndDs[index].D
        stuff.repairWindow = LAndDs[index].repairWindow
        stuff.fecOverhead = LAndDs[index].fecOverhead
        index++
        return stuff
    })

    let yourstuff = splitted.map(e => {
        let entry = {}
        entry.L = e.L
        entry.D = e.D
        entry.repairWindow = e.repairWindow
        entry.fecOverhead = e.fecOverhead

        entry.beforeAvgLoss = e.map( f => f.before.lost)
            .filter(f => f)
            .length / e.length * 100.0

        entry.afterAvgLoss = e.map( f => f.after.lost)
            .filter(f => f)
            .length / e.length * 100.0

        entry.avgLatency = e.map(f => f.after.timestamp - f.before.timestamp)
            .filter(f => !isNaN(f))
            .reduce((acc, g) => acc + g, 0) / e.length


        return entry
    })


    yourstuff.forEach(e => {
        console.log(JSON.stringify(e))
    })


/*
    Object.values(values).forEach(e => {
        console.log(e.ssrc)
    })
    Object.values(values2).forEach(e => {
        console.log(e.ssrc)
    })
    Object.values(values3).forEach(e => {
        console.log(e.ssrc)
    })
*/
    //console.log(myMap[575486318].ssrc)
}

function getValues(packets) {
    let values = packets.filter(e => getPayloadType(e) === 96)
    let myMap = {}

    values.forEach(packet => {
        const ssrc = getSSRC(packet)

        if (!myMap[ssrc]) {
            myMap[ssrc] = {ssrc, packets: []}
        }
        myMap[ssrc].packets.push(packet)
    })
    return myMap
}

function getStuff(packets1, packets2, packets3) {
    return packets1.map(e => {    //Husk at det er rar loss, at vi kanskje må loope fra 0, neida, vi tar kun loss for det vi faktisk sendte
        const entry = {seq: getSeq(e), before: {lost: true, timestamp: NaN}, after: {lost: true, timestamp: NaN}}

        const beforePacket = packets2.find(f => getSeq(e) === getSeq(f))
        const afterPacket = packets3.find(f => getSeq(e) === getSeq(f))
        if (beforePacket) {
            entry.before.lost = false
            entry.before.timestamp = getTimestamp(beforePacket)
        }
        if (afterPacket) {
            entry.after.lost = false
            entry.after.timestamp = getTimestamp(afterPacket)
        }
        return entry
    })
}

function getSeq(packet) {
    return packet.data.readUInt16BE(rtpOffset + 2)
}

function getTimestamp(packet) {
    return (packet.header.timestampSeconds + (packet.header.timestampMicroseconds / 1000000.0)) * 1000.0
}
