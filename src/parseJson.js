const fs = require('fs')

/*
let index = 0
const jsonObjects = fs.readFileSync(process.argv[2], "utf8").split("\n").map(e =>{
    return JSON.parse(e)
    console.log(index)
    index++
})*/

/*
const jsonObjects = fs.readFileSync(process.argv[2], "utf8").split("\n")
jsonObjects.forEach(e => {
    console.log(JSON.parse(e));
})*/

const jsonObjects = fs.readFileSync(process.argv[2], "utf8").split("\n")
const sliced = jsonObjects.slice(0, jsonObjects.length -2).map(e => JSON.parse(e))

const orderByPacketLoss = sliced.sort((e1, e2) => (e1.afterAvgLoss / e1.beforeAvgLoss) - (e2.afterAvgLoss / e2.beforeAvgLoss)).slice(0, 30)
const orderByOverhead = sliced.sort((e1, e2) => e1.fecOverhead - e2.fecOverhead).slice(0, 30)
const orderByRepairWindow = sliced.sort((e1, e2) => e1.repairWindow - e2.repairWindow).slice(0, 30)

const formattedPacketLoss = orderByPacketLoss.map(e => {
    return "\\hline " + e.L + " & " + e.D + " & " + e.beforeAvgLoss.toFixed(2) + "\\% & " + e.afterAvgLoss.toFixed(2) + "\\% & " + Math.ceil(e.repairWindow) + "ms & " + (e.fecOverhead * 100.0).toFixed(2) + "\\% \\\\"
})
const formattedOverhead = orderByOverhead.map(e => {
    return "\\hline " + e.L + " & " + e.D + " & " + e.afterAvgLoss.toFixed(2) + "\\% & " + Math.ceil(e.repairWindow) + "ms & " + (e.fecOverhead * 100.0).toFixed(2) + "\\% \\\\"
})
const formattedRepairWindow = orderByRepairWindow.map(e => {
    return "\\hline " + e.L + " & " + e.D + " & " + e.afterAvgLoss.toFixed(2) + "\\% & " + Math.ceil(e.repairWindow) + "ms & " + (e.fecOverhead * 100.0).toFixed(2) + "\\% \\\\"
})

const formattedOrderedByLAndD = sliced.map(e => {
    return "\\hline " + e.L + " & " + e.D + " & " + e.beforeAvgLoss.toFixed(2) + "\\% & " + e.afterAvgLoss.toFixed(2) + "\\% & " + Math.ceil(e.repairWindow) + "ms & " + (e.fecOverhead * 100.0).toFixed(2) + "\\% \\\\"
})

const x = sliced.sort((e1, e2) => e1.fecOverhead - e2.fecOverhead).map( e => parseFloat((e.fecOverhead * 100.0).toFixed(2)))
/*console.log("[")
x.forEach(e => {
    console.log(e + ",")
})
console.log("]");*/

const y = sliced.sort((e1, e2) => e1.fecOverhead - e2.fecOverhead).map( e => Math.ceil(e.repairWindow))

/*console.log("[")
y.forEach(e => {
    console.log(e + ",")
})
console.log("]")*/

const superCsv = sliced.sort((e1, e2) => e1.fecOverhead - e2.fecOverhead).map( e => parseFloat((e.fecOverhead * 100.0).toFixed(2)) + ";" + Math.ceil(e.repairWindow))
//superCsv.forEach(e => console.log(e))


let allPacketLoss = sliced.sort((e1, e2) => (e1.afterAvgLoss / e1.beforeAvgLoss) - (e2.afterAvgLoss / e2.beforeAvgLoss))

const allPacketsLostBefore = jsonObjects.slice(0, jsonObjects.length -2).map(e => JSON.parse(e)).map(e => e.beforeAvgLoss)
const allPacketsLostAfter = jsonObjects.slice(0, jsonObjects.length -2).map(e => JSON.parse(e)).map(e => e.afterAvgLoss)


/*for (i = 0; i < allPacketsLostBefore.length; i++) {
    console.log(allPacketsLostBefore[i] + "," + allPacketsLostAfter[i])
}*/


//allPacketsLostAfter.sort((e1, e2) => e1 - e2).forEach(e => console.log(e))




/*
//For excel 3d stuff

let cluster = excel3d(allPacketLoss)
cluster.forEach(e => {
    console.log(e + ",")
})
*/

//For excel repair window stuff

jsonObjects.slice(0, jsonObjects.length -2).map(e => JSON.parse(e))
.map(e => {
    return "\\hline " + e.L + " & " + e.D + " & " + e.beforeAvgLoss.toFixed(2) + "\\% & " + e.afterAvgLoss.toFixed(2) + "\\% & " + Math.ceil(e.repairWindow) + "ms & " + (e.fecOverhead * 100.0).toFixed(2) + "\\% \\\\"
})
.forEach(e => {
    //console.log(e)
})


let cluster = excel3dRepairWindow(sliced)
cluster.forEach(e => {
    console.log(e + ",")
})

//20x74


let transposedCluster = excel3dRepairWindow(sliced)

let array = [
    [1,2,3],
    [4,5,6]
]
let lol = transposedCluster[0].map((col, i) => transposedCluster.map(row => row[i])).reverse()

lol.forEach(e => {
    //console.log(e + ",")
})

for (D = 0; D <= 74; D++) {
    for (L = 0; L <= 20; L++) {

    }
}


for (D = 0; D <= 20; D++) {
    let row = []
    for (L = 0; L <= 74; L++) {
        //row.push(transposedCluster[D][L])
    }
    transposedCluster
}
let arr = []
for (D = 0; D <= 74; D++) {
    for (L = 0; L <= 20; L++) {

    }
}

let stdDev = myVarianceAndStdDev([1, 4, 8, 12, 19, 21])

printHeatmapPacketLoss(sliced)

//console.log(stdDev);


/*formattedOrderedByLAndD.forEach(e => {
    console.log(e)
})*/


//Til latex:
//formattedPacketLoss.forEach(e => console.log(e))
//formattedOverhead.forEach(e => console.log(e))
//formattedPacketLoss.forEach(e => console.log(e))




//console.log(formattedPacketLoss)
//console.log(orderByOverhead.map(e => e.L + " " + e.D + " " + e.fecOverhead))
//console.log(orderByRepairWindow.map(e => e.L + " " + e.D + " " + e.repairWindow))
/*console.log(JSON.stringify(

))
*/


function excel3d(allPacketLoss) {
    let cluster = []
    for (D = 0; D <= 74; D++) {
        let row = []
        for (L = 0; L <= 20; L++) {
            if (D === 0) {
                row.push(L)
                continue
            }
            else if (L === 0) {
                row.push(D)
                continue
            }

            const stuff = allPacketLoss.find(e => (e.L === L && e.D === D))
            if (stuff) {
                row.push(stuff.afterAvgLoss)
            }
            else {
                row.push(NaN)
            }
        }
        cluster.push(row)
    }
    return cluster
}

function excel3dRepairWindow(allRepairWindow) {
    let cluster = []
    for (D = 0; D <= 74; D++) {
        let row = []
        for (L = 0; L <= 20; L++) {
            if (D === 0) {
                row.push(L)
                continue
            }
            else if (L === 0) {
                row.push(D)
                continue
            }

            const stuff = allPacketLoss.find(e => (e.L === L && e.D === D))
            if (stuff) {
                row.push(Math.ceil(stuff.repairWindow))
            }
            else {
                row.push(0)
            }
        }
        cluster.push(row)
    }
    return cluster
}

function transpose(arr) {



    for (col = 0; col <= 74; col++) {
        for (row = 0; row <= 20; row++) {

        }
    }
}







function myVarianceAndStdDev(arr) {
  const mean = arr.reduce((acc, e) => acc + e, 0) / arr.length
  const squaredDiff = arr.map(e => Math.pow(e - mean, 2))
  const variance = squaredDiff.reduce((acc, e) => acc + e, 0) / squaredDiff.length
  const stddev = Math.sqrt(variance)

  return {stddev, variance}
}

function printHeatmapRepair(LAndDs) {
    for (D = 0; D <= 74; D++) {
        for (L = 0; L <= 20; L++) {

            const stuff = LAndDs.find(e => (e.L === L && e.D === D))
            if (stuff) {
                console.log(L + ' ' + D + ' ' + Math.ceil(stuff.repairWindow))
            }
            else {
                console.log(L + ' ' + D + ' ' + 0)
            }
        }
    }
}

function printHeatmapPacketLoss(LAndDs) {
    for (D = 0; D <= 74; D++) {
        for (L = 0; L <= 20; L++) {

            const stuff = LAndDs.find(e => (e.L === L && e.D === D))
            if (stuff) {
                console.log(L + ' ' + D + ' ' + stuff.afterAvgLoss.toFixed(2))
            }
            else {
                console.log(L + ' ' + D + ' ' + 'nan')
            }
        }
    }
}

function calcStatisticsForPackets(L, D, packets) {  //Husk at denne bare er for 111, for 222 må fec cluster være L * D + L + D - 1
    //Seperate stream into clusters
    let clusters = []
    let index = getSeqNum(packets[0])
    let offset = L * D
    //Remember wrap around

    while (true) {  //This is not safe for streams where seqnums are repeated. should be ok for our tests.
        let cluster = [L * D] //wrap around!!//wrap around!!//wrap around!!
        for (i = 0; i < (L*D); i++) {
            let packet = findBySeqnum(index + i)   //Should return null if no seqnum was found
            if (packet) {
                cluster[index + i] = packet
            }
        }
        clusters.push(cluster)

        index = ((index + (L * D)) % 65536) //HMMM
    }



    //

}
