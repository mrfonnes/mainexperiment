const {parsePcap, writePcap} = require('./myPcapParser.js')
const { getLAndDValues, getRTPStream } = require('./pcaputils')

/*--max_old_space_size=<size>*/



splitPcap(process.argv[2], process.argv[3])

async function splitPcap(smallPcapFileName, bigPcapFileName) {
    const smallPcap = await parsePcap(smallPcapFileName)

    let values = getLAndDValues(smallPcap.packets)
/*
    for (let L = 1; L <= 20; L++) {
        for (let D = 1; D <= 255; D++) {
            const clusters = makeClusters(L, D, smallPcap.packets)
            const repairWindow = calcAvg(clusters)

            let fecOverhead = calculateOverhead(L, D)
            if (repairWindow < 500.0 && fecOverhead < 0.67) {
                values.push({L, D, repairWindow, fecOverhead})
            }
        }
    }*/
    const bigPcap = await parsePcap(bigPcapFileName)

    let index = 0
    let fecConfigIndex = 0;

    while (index < bigPcap.packets.length) {
        const rtpStream = getRTPStream(bigPcap.packets.slice(index))
        const fecConfig = values[fecConfigIndex]

        let outputFile = '../../thisisthefile/beforeFEC/' + fecConfig.L + 'x' + fecConfig.D + '.pcap'
        await writePcap(outputFile, bigPcap.globalHeader, rtpStream)

        index += rtpStream.length
        console.log(index)

        fecConfigIndex++
    }
}
