const { getLAndDValues } = require('./pcaputils')
const { parsePcap } = require('./myPcapParser')

printLAndD()

async function printLAndD() {
    const {packets} = await parsePcap(process.argv[2])
    console.log(getLAndDValues(packets).length)
}
