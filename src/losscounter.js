const { getSSRC, getPayloadType } = require('./pcaputils')
const { parsePcap } = require('./myPcapParser')

countLoss(process.argv[2], process.argv[3], process.argv[4])

async function countLoss(pcap1, pcap2, pcap3) {
    const values1 = await getSSRCStreamsFromPcap(pcap1)
    console.log("done with 1")
    const values2 = await getSSRCStreamsFromPcap(pcap2)
    console.log("done with 2")
    const values3 = await getSSRCStreamsFromPcap(pcap3)
    console.log("done with 3")

    Object.entries(values1)
    .sort(([key1, value1], [key2, value2]) => value1.index - value2.index)
    .forEach(([ssrc, value]) => {
        const afterFECLoss = 1 - (values3[ssrc].packets.length / value.packets.length)
        const beforeFECLoss = 1 - (values2[ssrc].packets.length / value.packets.length)

        console.log(beforeFECLoss + ',' + afterFECLoss)
    })
}

function getSSRCStreams(packets) {
    let values = packets.filter(e => getPayloadType(e) === 96)
    let myMap = {}

    values.forEach(packet => {
        const ssrc = getSSRC(packet)

        if (!myMap[ssrc]) {
            myMap[ssrc] = {ssrc, packets: [], index: Object.keys(myMap).length}
        }
        myMap[ssrc].packets.push(packet)
    })
    return myMap
}

async function getSSRCStreamsFromPcap(pcap) {
    const {packets} = await parsePcap(pcap)
    return getSSRCStreams(packets)
}
