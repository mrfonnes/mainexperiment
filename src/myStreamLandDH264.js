const sleep = require('sleep')
const { execSync } = require('child_process')
const { parsePcap } = require('./myPcapParser.js')
const { getLAndDValues } = require('./pcaputils')

streamLAndDH264(process.argv[2], process.argv[3], process.argv[4])

async function streamLAndDH264(pcapFileName, executable, videoFileName) {
    const {packets} = await parsePcap(pcapFileName)
    const LAndDValues = getLAndDValues(packets)

    LAndDValues.forEach(({L, D}) => {
        console.log('L: ' + L + ' D: ' + D);
        execSync(`${executable} ${L} ${D} ${videoFileName}`)
        sleep.msleep(10000)
    })
}
