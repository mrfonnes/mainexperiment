const fs = require('fs')
const gilbert = require('./gilbert.js')

const testFolder = '../../thisisthefile/test/'
const outputFolder = '../../thisisthefile/testLoss3/'

const probabilityLoss = process.argv[2] /*0.838026*/

fs.readdirSync(testFolder)
    .filter(fileName => fileName.includes('.pcap'))
    .forEach(fileName => {
        const [L, D] = fileName.split('.')[0].split('x')
        const skip = (L * D) + (parseInt(L) + parseInt(D)) /*Her må jeg trekke fra 1!!!*/
        gilbert(testFolder + fileName, outputFolder + fileName.split('.')[0] + 'Loss.pcap', probabilityLoss, skip)
    })
