const fs = require('fs')

const jsonObjects1 = fs.readFileSync(process.argv[2], "utf8").split("\n")
const jsonObjects2 = fs.readFileSync(process.argv[3], "utf8").split("\n")
const jsonObjects3 = fs.readFileSync(process.argv[4], "utf8").split("\n")

const jsonObjects4 = fs.readFileSync(process.argv[5], "utf8").split("\n")

const sliced1 = jsonObjects1.slice(0, jsonObjects1.length -1).map(e => JSON.parse(e))
const sliced2 = jsonObjects2.slice(0, jsonObjects2.length -1).map(e => JSON.parse(e))
const sliced3 = jsonObjects3.slice(0, jsonObjects3.length -1).map(e => JSON.parse(e))

const sliced4 = jsonObjects3.slice(0, jsonObjects4.length -1).map(e => JSON.parse(e))

for (i = 0; i < sliced1.length; i++) {
    const avgBefore = (sliced1[i].beforeAvgLoss + sliced2[i].beforeAvgLoss + sliced3[i].beforeAvgLoss) / 3.0
    const avgAfter = (sliced1[i].afterAvgLoss + sliced2[i].afterAvgLoss + sliced3[i].afterAvgLoss) / 3.0

    const avgAfterOld = sliced4[i].afterAvgLoss

    const diff = Math.abs(avgAfterOld - avgAfter)
    console.log(diff)
}
