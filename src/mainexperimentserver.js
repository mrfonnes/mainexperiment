const net = require('net')
const sleep = require('sleep')
const { execSync } = require('child_process')
const { parsePcap } = require('./myPcapParser')
const { makeClusters, calculateOverhead } = require('./pcaputils')

const { getLAndDValues } = require('./pcaputils')

const PORT = 9876;

runMySocket(process.argv[2])

async function runMySocket(pcap) {
    console.log('Reading stuff...')
    const {packets} = await parsePcap(pcap)
    let fecConfigs = getLAndDValues(packets)

    console.log('Done stuff...')

    net.createServer(sock => {
        let index = 0

        console.log('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);

        sock.on('data', data => {
            const fromClient = data.toString('utf8')
            const fecConfig = fecConfigs[index]
            if (fromClient === "Hello") {
                if (index >= fecConfigs.length) {
                    sock.write(JSON.stringify("exit"));
                    socket.destory()
                }
                sleep.sleep(1)
                sock.write(JSON.stringify(fecConfig));
            }
            else if (fromClient === "Ok") {
                sleep.sleep(2)
                console.log('started streaming');
                execSync(`bittwist -i 1 ../../thisisthefile/testLoss/${fecConfig.L}x${fecConfig.D}Loss.pcap`)
                console.log("waiting for hello")
                index++
            }
        })

        // Add a 'close' event handler to this instance of socket
        sock.on('close', data => {
            console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
        })

    }).listen(PORT)

}




/*
console.log('Reading stuff...')
getLAndDValues(process.argv[2]).then(fecConfigs => {
    console.log('Done stuff...')
    net.createServer(sock => {
        let index = 0

        console.log('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);

        sock.on('data', data => {
            const fromClient = data.toString('utf8')
            const fecConfig = fecConfigs[index]
            if (fromClient === "Hello") {
                if (index >= fecConfigs.length) {
                    sock.write(JSON.stringify("exit"));
                    socket.destory()
                }
                sleep.sleep(1)
                sock.write(JSON.stringify(fecConfig));
            }
            else if (fromClient === "Ok") {
                sleep.sleep(2)
                console.log('started streaming');
                execSync(`bittwist -i 1 ../../thisisthefile/testLoss/${fecConfig.L}x${fecConfig.D}Loss.pcap`)
                console.log("waiting for hello")
                index++
            }
        })

        // Add a 'close' event handler to this instance of socket
        sock.on('close', data => {
            console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
        })

    }).listen(PORT);
})

async function getLAndDValues(input) {
    const pcap = await parsePcap(input)
    let values = []

    for (let L = 1; L <= 20; L++) {
        for (let D = 1; D <= 255; D++) {
            const clusters = makeClusters(L, D, pcap.packets)
            const repairWindow = calcAvg(clusters)

            let fecOverhead = calculateOverhead(L, D)
            if (repairWindow < 500.0 && fecOverhead < 0.67) {
                values.push({L, D, repairWindow, fecOverhead})
            }
        }
    }
    return values
}
*/
