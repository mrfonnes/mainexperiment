const fs = require('fs')

const csvFiles = process.argv.slice(2).map(e => {
    return fs.readFileSync(e, "utf8").split("\n")
    .filter(e => e.trim())
    .map(e => e.split(",").map(parseFloat))
})

let before = Array(csvFiles[0].length).fill(0)
let after = Array(csvFiles[0].length).fill(0)

for (let i = 0; i < csvFiles[0].length; i++) {
    for (j = 0; j < csvFiles.length; j++) {
        before[i] += csvFiles[j][i][0]
        after[i] += csvFiles[j][i][1]
    }
    before[i] = (before[i] / csvFiles.length) * 100.0
    after[i] = (after[i] / csvFiles.length) * 100.0
}

before.forEach((e, i) => {
    console.log(e + ',' + after[i]);
})
