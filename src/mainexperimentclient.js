var net = require('net');
const sleep = require('sleep')
const { execSync } = require('child_process');

var PORT = 9876;

var client = new net.Socket()
const HOST = process.argv[2]
const exec = process.argv[3]

client.connect(PORT, HOST, function() {
    console.log('CONNECTED TO: ' + HOST + ':' + PORT);
    client.write('Hello');
})

client.on('data', function(data) {
    let fecConfig = JSON.parse(data.toString('utf8'))
    client.write('Ok');
    console.log(`starting receiving with ${fecConfig.L} ${fecConfig.D} ${parseInt(Math.ceil(fecConfig.repairWindow))}`)
    execSync(`${exec} 232.126.107.222 ${fecConfig.L} ${fecConfig.D} ${parseInt(Math.ceil(fecConfig.repairWindow))}`)
    console.log("Done receiving");
    client.write('Hello');
})

client.on('close', function() {
    console.log('Connection closed');
})
